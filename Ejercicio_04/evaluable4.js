var contador=0;
var intervalo;
var cuentaDados=[0,0,0,0,0,0]; // Vector para ir contando cuantas veces sale cada dado
var listaTiradas="";

function generaNumero() {
	dado = parseInt(Math.random() * 6) + 1;
	return dado;
}

function muestraDado(num) {
	switch (num) {
		case 0:
			imgDado.src="dado-0.jpg";
			break;
		case 1:
			imgDado.src="dado-1.jpg";
			break;
		case 2:
			imgDado.src="dado-2.jpg";
			break;
		case 3:
			imgDado.src="dado-3.jpg";
			break;
		case 4:
			imgDado.src="dado-4.jpg";
			break;
		case 5:
			imgDado.src="dado-5.jpg";
			break;
		case 6:
			imgDado.src="dado-6.jpg";
			break;
	}
}

function tiraDado() {
	contador=contador+1;
	if (contador<=20) {
		num=generaNumero();
		cuentaDados[num-1]++;
		listaTiradas+=num.toString();
		muestraDado(num);
	} else {
		clearInterval(intervalo);
		max=cuentaDados[0];
		maxPos=0;
		for (i=1;i<6;i++) {
			if (cuentaDados[i]>max) {
				max=cuentaDados[i]; 
				maxPos=i; // La posición es la que me dirá qué dado es el que se ha contado más veces
			}
		}
		maximo=maxPos+1; // Le sumo 1 porque en la posición cero cuento 'unos', en la uno 'doses', etc.
		alert("El número que más veces ha salido es: "+maximo.toString()+ " un total de "+ max.toString()+" veces");
		alert("Lista de tiradas: "+ listaTiradas);
	}
}

 function comenzar() {
	 intervalo=setInterval("tiraDado()",1000);
 }   

   
window.onload = function() {
    imgDado=document.getElementById("dado");
    btnComenzar=document.getElementById("comenzar");
    btnComenzar.addEventListener("click",comenzar, false);
}
