function devuelveSimbolosBase(base) {
    simbolos="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    return simbolos.substring(0,base);
}

function compruebaNumeroBase(num, base) {
    simbolosValidos=devuelveSimbolosBase(base).toUpperCase();
    for (i=0;i<num.length;i++) {
        car=num[i];
        if (simbolosValidos.indexOf(car.toUpperCase())==-1) return false
    }
    return true;
}

function reverse(s) {
    return s.split("").reverse().join("");
}

function devuelveValor(simbolo) {
    simbolos="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    return simbolos.indexOf(simbolo.toUpperCase());    
}

function devuelveSimbolo(valor) {
    simbolos="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    return simbolos.charAt(valor);
}

function pasarBase10(numero,base) {
    
    var strNumero=numero.toUpperCase();
    var numDigitos=strNumero.length;
    var numBase10=0;
    
    strNumero=reverse(strNumero);
    for (i=0;i<numDigitos;i++) {
        digito=strNumero.charAt(i);
        if (digito!="0") {
            valor=devuelveValor(digito);
            potencia=valor*Math.pow(base,i);
            numBase10+=potencia;
        }
    }
    
    return numBase10
}

function pasarBaseN(numero,base) {
    cociente=numero
    divisor=numero
    resultado=""
    while (cociente>=base) {
        cociente=parseInt(divisor/base)
        resto= divisor % base
        divisor=cociente
        resultado=devuelveSimbolo(resto) + resultado
    }
    resultado=devuelveSimbolo(cociente) + resultado
    return resultado
}
    
function convierte() {
    baseI=parseInt(txtBaseI.value);
    baseF=parseInt(txtBaseF.value);
    strNum=txtNumero.value;
    if (!compruebaNumeroBase(strNum,baseI)) {
        alert("El número no es correcto. Los símbolos permitidos en esa base son: '" + devuelveSimbolosBase(baseI)+"'");
        txtNumero.value="";
        txtNumero.focus();
        return "Error"
    }
    numDecimal=pasarBase10(strNum,baseI);
    res=pasarBaseN(numDecimal,baseF);
    txtResultado.value=res;
    
}
   
window.onload = function() {
    txtBaseI=document.getElementById("basei");    
    txtBaseF=document.getElementById("basef");    
    txtNumero=document.getElementById("numero");  
    txtResultado=document.getElementById("resultado")
    btnConvertir=document.getElementById("convertir");
    btnConvertir.addEventListener("click",convierte, false);
}
