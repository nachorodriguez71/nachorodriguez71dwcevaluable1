var estadoInsercion="sobreEscribir";
var comaPulsada=false;
var strPantalla="";
var estado="insertando";
var operacion="";
var operando1=0;
var operando2=0;

function inicializa() {
    estadoInsercion="sobreEscribir";
    comaPulsada=false;
    estado="insertando";
    operacion="";
    operando1=0;
    operando2=0;    
}
    
function vaciaJugadores() {
    selJugadores.options.length=1;
}


function botonDigitos(num) {
	// si la pantalla está a cero y metemos otro cero no hacemos nada para evitar
	// ceros a la izquierda, excepto si se ha pulsado coma
    if (num==0 && !comaPulsada && strPantalla=="0") {
        estadoInsercion="sobreEscribir"; // Cuando haya un cero y metamos otro dígito se sobreescribirá el 0 (salvo si hay coma)
    } else {
        if (estadoInsercion=="sobreEscribir") {
            strPantalla=num.toString();
            if (num!=0) estadoInsercion="insertar";
        } else {
            strPantalla+=num.toString();
        }
        txtPantalla.value=strPantalla;
    }
}

function botonComa() {
    if (!comaPulsada) {
        comaPulsada=true;
        if (estadoInsercion=="sobreEscribir") {
            estadoInsercion="insertar"    
            strPantalla="0.";            
        } else {
            strPantalla+=".";    
        }
        txtPantalla.value=strPantalla;
    }
}

function botonCua() {
    if (estado!="operando") {
        num=parseFloat(txtPantalla.value);
        resultado=Math.pow(num,2);
        txtPantalla.value=resultado;
        estadoInsercion="sobreEscribir";
        comaPulsada=false;
    }
}

function botonSqr() {
    if (estado!="operando") {
        num=parseFloat(txtPantalla.value);
        resultado=Math.sqrt(num);
        txtPantalla.value=resultado;
        estadoInsercion="sobreEscribir";
        comaPulsada=false;  
    }
}

function operando() {
        estado="operando"
        operando1=parseFloat(txtPantalla.value)
        estadoInsercion="sobreEscribir";
        comaPulsada=false;  
}

function botonMas() {
    if (estado!="operando") {
        operacion="suma";
        operando();
    }
}

function botonRes() {
    if (estado!="operando") {
        operacion="resta";
        operando();
    }
}

function botonPro() {
    if (estado!="operando") {
        operacion="producto";
        operando();
    }
}

function botonDiv() {
    if (estado!="operando") {
        operacion="division";
        operando();
    }
}

function botonIgual() {
    operando2=parseFloat(txtPantalla.value)
    switch (operacion) {
        case "suma":
            resultado=operando1 + operando2;
            break;
        case "resta":
            resultado=operando1 - operando2;
            break;
        case "division":
            if (operando2!=0) {
                resultado=operando1 / operando2;    
            } else {
                resultado="Div by zero!";
            }
            break;
        case "producto":
            resultado=operando1 * operando2;
            break;            
    }
    txtPantalla.value=resultado;
    inicializa();
}

window.onload = function() {
    lista=["Coma","Sqr","Cua","Mas","Pro","Div","Res","Igual"];
    txtPantalla=document.getElementById("pantalla");

    // Para no tener que escribir varias veces lo mismo usamos "eval"
    
    for (i in lista) {
        eval("btn"+lista[i]+"=document.getElementById('b"+lista[i]+"');");
        eval("btn"+lista[i]+".addEventListener('click',boton"+lista[i]+");");
    }

    for (i=0;i<=9;i++) {
        eval("btnB"+i+"=document.getElementById('b"+i+"');");  
        //Manera de poder pasar parámetros es usando una función anónima
        eval("btnB"+i+".addEventListener('click',function(){botonDigitos("+i+");});");
    } 
    
    
}
